package page.danya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import page.danya.models.Mark;

public interface MarkRepository extends JpaRepository<Mark, Integer> {



}
